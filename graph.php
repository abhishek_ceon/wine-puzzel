<?php
 /**
 * This class is used to implement the bipartite matching algorithms.
 *
 * @author	Abhishek Singh <abhishek.ceon@gmail.com>
 * @createdDate	26 March 2017
 */	
class Graph {
	private $graph;
	private $wines;

	
    function __construct($graph) 
    {
        /** Graph is in an associative array
			 Array(
			   'person1' => ['wine1', 'wine2'],
			   'person2' => ['wine2', 'wine3']
			)
		*/
        $this->graph = $graph;
	}

 /**
 * This function will be used to set the wines List.
 *
 * @author	Abhishek Singh <abhishek.ceon@gmail.com>
 * @params	array	$wine_list wines list.
 * @return  NA.
 */	
    public function set_wine_list($wine_list)
    {
        $this->wines = $wine_list;
	}

 /**
 * This function also  used to set the wines List from person's choices data.
 *
 * @author	Abhishek Singh <abhishek.ceon@gmail.com>
 * @return  array	wine_list	wines list
 */
    private function _enumerate_wines()
    {
        $wine_list = [];

		foreach ($this->graph as $person => $person_wine_list) {
			foreach ($this->graph as $wine_name) {
				if (!in_array($wine_name, $wine_list)) {
					$wine_list[] = $wine_name;
				}
			}
		}

        return $wine_list;
	}

 /**
 * This function is ued to allocate wines to specific person.
 *
 * @author	Abhishek Singh <abhishek.ceon@gmail.com>
 * @param	string $person_name
 * @param	obj	&$bpm_array
 * @param	obj	&$seen_wines
 * @return  boolean		true/false
 */
    private function allocateWines($person_name, &$bpm_array, &$seen_wines) 
    {
        foreach ($this->wines as $wine_name) {
            // If person is interested in wine, and wine has not been visited
            if (in_array($wine_name, $this->graph[$person_name]) && !in_array($wine_name, $seen_wines)) {
                $seen_wines[] = $wine_name;

                // If wine has not been assigned to any other person then assign it to this person
                // Or if wine has been assigned to some other person, then
                // check if that person can be assigned to some other wine.
                // If other person can be assigned to other wine then assign this wine to this person,
                // other person would have been assigned to other wine in recursion call
                if (! array_key_exists($wine_name, $bpm_array) || $this->bpm($bpm_array[$wine_name], $bpm_array, $seen_wines)) {
                    $bpm_array[$wine_name] = $person_name;

                    return true;
				}
				
                return false;
			}
		}
	}

 /**
 * This function will be  ued to get the final wines allocated lists
 *
 * @author	Abhishek Singh <abhishek.ceon@gmail.com>
 * @return  array	$bpm_array; final array list
 */
    public function get_final_array()
    {
        /**
			Returns a dict where key is index of wine and value in index of person,
			Total number of wine can be sold is length of this dict
			This is implemented using maximum bipartite matching algorithms
        **/
        $bpm_array = array();

        if(empty($this->wines)) {
            $this->set_wine_list($this->enumerate_wines());
		}

        foreach ($this->graph as $person_name => $wine_list) {
            $seen_wines = array();
            $this->allocateWines($person_name, $bpm_array, $seen_wines);
		}

        return $bpm_array;
	}


 /**
 * This function will be  ued to generate list from file.
 *
 * @author	Abhishek Singh <abhishek.ceon@gmail.com>
 * @param	string	$file file name with path
 * @return  obj	$graph graph object
 */
    public static function from_file($file) 
    {
        $person_wine_matrix = array();
        $wine_names = array();

        foreach (file($file) as $line) {
			$line_number = 0;
			$line = trim($line);
			$line_number++;

			if (!empty($line)) {
				$line_list = explode("\t", $line);

				if(count($line_list) != 2) {
					 throw new Exception("Line at line number " . $line_number . " must have two  columns separated by tab");
				}

				$person_name = trim($line_list[0]);
				$wine_name = trim($line_list[1]);

				if (!array_key_exists($person_name, $person_wine_matrix)) {
					$person_wine_matrix[$person_name] = array();
				}

				$person_wine_matrix[$person_name][] = $wine_name;

				if (! in_array($wine_name, $wine_names)) {
					$wine_names[] = $wine_name;
				}
			}
		}					

        $graph = new self($person_wine_matrix);
        $graph->set_wine_list($wine_names);

        return $graph;
	}
}
