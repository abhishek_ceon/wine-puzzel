<?php
// This file contain the implementation/solution  of Wine Puzzel.
ini_set('display_errors', 'On');
ini_set('max_execution_time', 0);
include "graph.php";

$input_file = "wine1.txt";
$output_file = "out.txt";

$graph = Graph::from_file($input_file);
$bpm_array = $graph->get_final_array();

$file = fopen($output_file, 'w+');

fwrite($file, count($bpm_array) . "\n");
foreach ($bpm_array as $wine => $person)
{
    fwrite($file, $wine . "\t" . $person . "\n");
}
fclose($file);
